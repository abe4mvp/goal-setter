class GoalsController < ApplicationController

  def create
    @goal = current_user.goals.new(params[:goal])
    create_and_redirect_for!(@goal, user_url(current_user))
  end

  def edit
    @goal = Goal.find(params[:goal][:id])
    redirect_to current_user
  end

  def update
    @goal = Goal.find(params[:goal][:id])
    if @goal.update_attributes(params[:goal])
      
    else
    end

  end

  def destroy
  end

end
