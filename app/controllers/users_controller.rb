class UsersController < ApplicationController
  before_filter :authenticate, only: [:show]
  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])

    create_and_redirect_for!(@user)
  end

  def show
    fail
    @user = User.find(params[:id])
  end

  def create_goal
    @goal = current_user.goals.new(params[:goal])
    create_and_redirect_for!(@goal, user_url(current_user))
  end

  def edit_goal
    @goal = Goal.find(params[:goal][:id])
    render :show
  end

  def update_goal
    @goal = Goal.find(params[:goal][:id])
    if @goal.update_attributes(params[:goal])
      redirect_to current_user
    else
      flash[:errors] = @goal.errors.full_messages
      render :show
    end

  end

  def destroy_goal
    @goal = Goal.find(params[:goal][:id])
    @goal.delete!
    render :show
  end



end
