class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.find_by_username_and_password(
      params[:user][:username],
      params[:user][:password]
    )

    create_and_redirect_for!(@user)
  end

  def destroy
    session[:token] = nil
    current_user.reset_token if logged_in?
    redirect_to new_session_url
  end
end
