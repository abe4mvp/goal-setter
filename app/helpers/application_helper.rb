module ApplicationHelper
  def current_user
    return nil unless session[:token]
    @current_user ||= User.find_by_token(session[:token])
  end

  def logged_in?
    !!current_user
  end

  def authenticate
    redirect_to new_session_url unless logged_in?
  end

  def create_and_redirect_for!(model, redirect_url = nil)
    if model && model.save
      session[:token] = model.reset_token if model.class == User
      redirect_to  redirect_url || current_user #all info on the show page
    else
      if model.nil? #implies no user found
        flash[:errors] =  ["Username or Password is wrong"]
      else
        flash[:errors] =  model.errors.full_messages
        render :new
      end
    end
  end

end
