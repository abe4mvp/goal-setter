class Goal < ActiveRecord::Base
  attr_accessible :title, :user_id, :public
  validates :title, :user_id, :presence => true
  validates :public, :inclusion => { :in => [true, false] }

  belongs_to :user
end
