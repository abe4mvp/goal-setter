GoalSetter::Application.routes.draw do
  resources :users do
    member do
      get "edit_goal"
      put "update_goal"
      post "create_goal"
      delete "destroy_goal"
    end
  end

  resource :session

  root to: "session#new"
end
