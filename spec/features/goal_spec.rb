require 'spec_helper'
require 'capybara'

describe "Goal" do
  before :each do
    sign_up_as_superman
  end

  describe "can be created, read, updated, destroyed" do
    it "should require a title" do
      create_goal("")
      page.should have_content "Title can't be blank"
    end

    it "should allow users to create a goal" do
      create_goal("!Lois Lane")
      page.should have_content "!Lois Lane"
    end

    it "should allow users to update a goal" do
      create_goal("!Lois Lane")
      click_button "Edit"
      fill_in "Title", with: "Stop Lex Luthor"
      page.should have_content "Stop Lex Luthor"
    end

    it "should allow users to destroy a goal" do
      create_goal("!Lois Lane")
      click_button "Delete"
      page.should_not have_content "!Lois Lane"
    end
  end

  describe "allows other users to see your public goals" do
    it "allows other users to see public goals" do
      create_goal("!Lois Lane")
      click "Log Out"
      sign_up("Batman")
      visit "/users/1"
      page.should have_content "!Lois Lane"
    end

    it "prevents other users from seeing private goals" do
      create_goal("Steal Bat-mobile", false)
      click "Log Out"
      sign_up("Batman")
      visit "/users/1"
      page.should_not have_content "Steal Bat-mobile"
    end

  end

  describe " can be complete or incomplete" do

    before :each do
      create_goal("Steal Bat-mobile", false)
    end
      it "displays the staus of an incomplete goal" do
        page.should have_content "Incomplete"
      end

    it "displays the status of a completed goal" do
      click_button "Complete"
      page.should have_content "Finished"
    end

  end

end