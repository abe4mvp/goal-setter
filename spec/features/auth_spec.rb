require 'spec_helper'
require 'capybara'

describe "the signup process" do

  it "has a new user page" do
    visit "/users/new"
    page.should have_content "Sign Up"
  end

  describe "signing up a user" do

    it "shows username on the homepage after signup" do
      sign_up_as_superman
      page.should have_content 'superman'
    end

  end

end

describe "signing in" do


  it "shows username on the homepage after sign in" do
    User.create!(username: "batman", password: "cryptonite")
    sign_in("batman")
    page.should have_content "batman"
  end

end

describe "signing out" do


  it "begins with signing out state" do
    visit '/users/1'
    page.should have_content "Sign In"
  end

  it "doesn't show username on the homepage after sign out" do
    sign_up_as_superman
    click_button 'Sign Out'
    page.should_not have_content "Superman"
  end

end